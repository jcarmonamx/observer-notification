//
//  BaseScreen.swift
//  Observer-Notification
//
//  Created by Juan Manuel García Carmona @jcarmonamx on 24/11/21.
//

import UIKit

/*
 * Creamos identificadores unicos para las notificaciones.
 * Es buena idea crear un archivo especial para manejar las claves y no mantenerlo de manera global como en este caso
 */
let lightNotificationKey = "mx.caffeina.lightSide"
let darkNotificationKey = "mx.caffeina.darkSide"

class BaseScreen: UIViewController {
    
    /*
     * MARK: Outlets
     */

    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var chooseButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    
    /*
     * MARK: Actions
     */

    @IBAction func chooseButtonTapped(_ sender: UIButton) {
        let selectionVC = storyboard?.instantiateViewController(withIdentifier: "SelectionScreen") as! SelectionScreen
        present(selectionVC, animated: true, completion: nil)
    }
    
    /*
     * MAR: Variavles
     */
    
    let dark = Notification.Name(rawValue: darkNotificationKey)
    let light = Notification.Name(rawValue: lightNotificationKey)
    
    /*
     * MARK: Funciones del sistema
     */
    
    // Cuando se trabaja con observadores es buena idea eliminarlos cuando ya no son necesarios
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        chooseButton.layer.cornerRadius = chooseButton.frame.size.height/2
        createObservers()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

/*
 * MARK: Funciones propias
 */

extension BaseScreen {
    
    /*
     * Funcion que crea los observadores para el light/dark side
     */
    func createObservers() {
        
        // Light
        NotificationCenter.default.addObserver(self, selector: #selector(updateCharacterImage(notification:)), name: light, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateNameLabel(notification:)), name: light, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateBackground(notification:)), name: light, object: nil)
        
        // Dark
        NotificationCenter.default.addObserver(self, selector: #selector(updateCharacterImage(notification:)), name: dark, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateNameLabel(notification:)), name: dark, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateBackground(notification:)), name: dark, object: nil)
    }
    
    @objc func updateCharacterImage(notification: NSNotification) {
        let isLight = notification.name == light
        let image = isLight ? UIImage(named: "luke")! : UIImage(named: "vader")!
        mainImageView.image = image
    }
    
    @objc func updateNameLabel(notification: NSNotification) {
        let isLight = notification.name == light
        let name = isLight ? "Luke Skywalker" : "Darth Vader"
        nameLabel.text = name
    }
    
    @objc func updateBackground(notification: NSNotification) {
        let isLight = notification.name == light
        let color = isLight ? UIColor.cyan : UIColor.red
        view.backgroundColor = color
    }
    
}
