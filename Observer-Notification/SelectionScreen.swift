//
//  SelectionScreen.swift
//  Observer-Notification
//
//  Created by Juan Manuel García Carmona @jcarmonamx on 24/11/21.
//

import UIKit

class SelectionScreen: UIViewController {
    
    /*
     * MARK: Funciones del sistema
     */
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    /*
     * MARK: Actions
     */
    
    @IBAction func imperialButtonTapped(_ sender: UIButton) {
        // Enviamos la notificación de que el botón imperial fue presionado
        let name = Notification.Name(rawValue: darkNotificationKey)
        NotificationCenter.default.post(name: name, object: nil)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func rebelButtonTapped(_ sender: UIButton) {
        // Enviamos la notificación de que el botón rebelion fue presionado
        let name = Notification.Name(rawValue: lightNotificationKey )
        NotificationCenter.default.post(name: name, object: nil)
        dismiss(animated: true, completion: nil)
    }
    
}
